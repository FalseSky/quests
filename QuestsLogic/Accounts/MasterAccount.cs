﻿using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public class MasterAccount : UserAccount
	{
		public MasterAccount( string _name, Verifier _verificator )
			:	base( _name, _verificator )
		{

		}


		#region Entity Framework

		protected MasterAccount()
		{
			
		}

		#endregion
	}
}