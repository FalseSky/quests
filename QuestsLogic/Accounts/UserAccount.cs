﻿using System.Collections.Generic;
using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public abstract class UserAccount
	{
		protected UserAccount( string _name, Verifier _verificator )
		{
			Name = _name;
			Verificator = _verificator;

			UserMessage = new List < Message >();
		}


		public void SetName( object _userKey, string _newName )
		{
			Verificator.ThrowIfKeyInvalid( _userKey );

			Name = _newName;
		}

		public void SetVerificator( object _userKey, Verifier _newVerificator )
		{
			Verificator.ThrowIfKeyInvalid( _userKey );

			Verificator = _newVerificator;
		}


		public void AddMessage( UserAccount _sender, string _messageText )
		{
			UserMessage.Add( new Message( _sender, _messageText ) );
		}

		public void RemoveMessage( object _userKey, Message _message )
		{
			Verificator.ThrowIfKeyInvalid( _userKey );

			UserMessage.Remove( _message );
		}


		public string Name { get; private set; }

		public virtual Verifier Verificator { get; private set; }

		public virtual ICollection < Message > UserMessage { get; private set; }


		#region Entity Framework

		protected UserAccount()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}