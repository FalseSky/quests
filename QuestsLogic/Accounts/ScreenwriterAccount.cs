﻿using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public class ScreenwriterAccount : UserAccount
	{
		public ScreenwriterAccount( string _name, Verifier _verificator )
			:	base( _name, _verificator )
		{

		}


		#region Entity Framework

		protected ScreenwriterAccount()
		{
			
		}

		#endregion
	}
}