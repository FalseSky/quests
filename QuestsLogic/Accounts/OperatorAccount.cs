﻿using System.Collections.Generic;
using System.Linq;
using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public class OperatorAccount : UserAccount
	{
		public OperatorAccount( string _name, Verifier _verificator )
			:	base( _name, _verificator )
		{
			ProblemReports = new LinkedList < ProblemReport >();
			PaymentReports = new LinkedList < PaymentReport >();
		}


		public void NewProblemReport( UserAccount _userAccount, string _problemDescription )
		{
			ProblemReports.Add( new ProblemReport( _userAccount, _problemDescription ) );
		}

		public void NewPayment( PlayerAccount _playerAccount, string _paymentDestination, decimal _paymentSize )
		{
			PaymentReports.Add( new PaymentReport( _playerAccount, _paymentDestination, _paymentSize ) );
		}


		public ProblemReport PopProblemReport( object _accountKey )
		{
			Verificator.ThrowIfKeyInvalid( _accountKey );

			if ( ! ProblemReportExist() )
				return null;

			ProblemReport problemReport = ProblemReports.First();
			ProblemReports.Remove( problemReport );

			return problemReport;
		}
		
		public PaymentReport PopPaymentReport( object _accountKey )
		{
			Verificator.ThrowIfKeyInvalid( _accountKey );

			if ( ! PaymentReportExist() )
				return null;

			PaymentReport paymentReport = PaymentReports.First();
			PaymentReports.Remove( paymentReport );

			return paymentReport;
		}


		public bool ProblemReportExist()
		{
			return ProblemReports.Any();
		}

		public bool PaymentReportExist()
		{
			return PaymentReports.Any();
		}


		public virtual ICollection < ProblemReport > ProblemReports { get; private set; }

		public virtual ICollection < PaymentReport > PaymentReports { get; private set; }


		#region Entity Framework

		protected OperatorAccount()
		{
			
		}

		#endregion
	}
}