﻿using System.Collections.Generic;
using QuestsLogic.Accounts.PlayerAccountExceptions;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public class PlayerAccount : UserAccount
	{
		public PlayerAccount( string _name, Verifier _verificator )
			:	base( _name, _verificator )
		{
			PaymentHistory = new List < PaymentReport >();
			EventHistory = new List < GameEvent >();
		}


		public void OnPaymentAccepted( PaymentReport _newPayment )
		{
			PaymentHistory.Add( _newPayment );
		}

		public void OnGameEventEnded( GameEvent _gameEvent )
		{
			EventHistory.Add( _gameEvent );
		}

		public void OnPlayerJoinToTeam( PlayersTeam _playersTeam )
		{
			PlayersTeam = _playersTeam;
		}


		public int PlayerRating { get; set; }

		public int TotalDiscount
		{
			get { return m_totalDiscount; }

			set
			{
				if ( value < 0 )
					throw new NegativeDiscountException();

				if ( value > 50 )
					throw new DiscountMoreThanFiftyException();

				m_totalDiscount = value;
			}
		}

		public virtual ICollection < PaymentReport > PaymentHistory { get; private set; }

		public virtual ICollection < GameEvent > EventHistory { get; private set; }

		public virtual PlayersTeam PlayersTeam { get; private set; }


		private int m_totalDiscount;
		

		#region Entity Framework

		protected PlayerAccount()
		{
			
		}

		#endregion
	}
}