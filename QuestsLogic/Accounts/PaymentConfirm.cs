﻿namespace QuestsLogic.Accounts
{
	public class PaymentConfirm
	{
		public PaymentConfirm( RegisteredUserAccount _reporter, PaymentConfirm _description )
		{
			m_reporter = _reporter;
		}


		private RegisteredUserAccount m_reporter;
		private string m_description;
	}
}