﻿using QuestsLogic.Tools;

namespace QuestsLogic.Accounts
{
	public class JudgeAccount : UserAccount
	{
		public JudgeAccount( string _name, Verifier _verificator )
			:	base( _name, _verificator )
		{

		}
		
		
		#region Entity Framework

		protected JudgeAccount()
		{
			
		}

		#endregion
	}
}