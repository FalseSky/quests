﻿using System;
using QuestsLogic.Accounts;

namespace QuestsLogic.Tools
{
	public class Message
	{
		public Message( UserAccount _userAccount, string _messageText )
		{
			Time = DateTime.Now;

			UserName = _userAccount.Name;
			MessageText = _messageText;
		}

		
		public string UserName { get; private set; }
		public DateTime Time { get; private set; }
		public string MessageText { get; private set; }


		#region Entity Framework

		protected Message()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}