﻿using QuestsLogic.Accounts;

namespace QuestsLogic.Tools
{
	public class ProblemReport
	{
		public ProblemReport( UserAccount _user, string _problemDescription )
		{
			UserName = _user.Name;
			ProblemDescription = _problemDescription;
		}
		

		public string UserName { get; private set; }
		public string ProblemDescription { get; private set; }


		#region Entity Framework

		protected ProblemReport()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}