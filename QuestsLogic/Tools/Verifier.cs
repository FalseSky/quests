﻿using QuestsLogic.Tools.VerifierExceptions;

namespace QuestsLogic.Tools
{
	public class Verifier
	{
		public Verifier( object _validKey )
		{
			if( _validKey == null )
				throw new NullValidKeyException();

			KeyHash = _validKey.GetHashCode();
		}


		public bool ValidKey( object _key )
		{
			return KeyHash == _key ?. GetHashCode();
		}

		public void ThrowIfKeyInvalid( object _key )
		{
			if( ! ValidKey( _key ) )
				throw new InvalidKeyException();
		}


		public int KeyHash { get; private set; }


		#region Entity Framework

		protected Verifier()
		{
			
		}

		public int Id { set; get; }

		#endregion
	}
}