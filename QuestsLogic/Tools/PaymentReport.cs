﻿using QuestsLogic.Accounts;

namespace QuestsLogic.Tools
{
	public class PaymentReport
	{
		public PaymentReport( PlayerAccount _playerAccount, string _paymentDestination, decimal _paymentSize )
		{
			UserName = _playerAccount.Name;
			Destination = _paymentDestination;
			Size = _paymentSize;
		}


		public string UserName { get; private set; }
		public string Destination { get; private set; }
		public decimal Size { get; private set; }


		#region Entity Framework

		protected PaymentReport()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}