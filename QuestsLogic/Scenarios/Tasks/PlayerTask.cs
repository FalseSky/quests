﻿using QuestsLogic.Tools;

namespace QuestsLogic.Scenarios.Tasks
{
	public class PlayerTask
	{
		public PlayerTask( string _description, Verifier _completeTaskVerificator )
		{
			Description = _description;
			CompleteTaskVerificator = _completeTaskVerificator;
		}

		
		public string Description { get; private set; }

		public virtual Verifier CompleteTaskVerificator { get; private set; }


		#region Entity Framework

		protected PlayerTask()
		{
			
		}
		
		public int Id { get; set; }

		#endregion
	}
}