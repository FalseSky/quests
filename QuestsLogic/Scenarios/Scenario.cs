﻿using System.Collections.Generic;
using QuestsLogic.Scenarios.ScenarioExceptions;
using QuestsLogic.Scenarios.Tasks;

namespace QuestsLogic.Scenarios
{
	public class Scenario
	{
		public Scenario( string _name )
		{
			Name = _name;

			MinimumPlayers = 1;
			MaximumPlayers = 10;

			PlayerTasks = new List < PlayerTask >();
		}


		public void AddPlayerTask( PlayerTask _newTask )
		{
			if( _newTask == null )
				throw new NullPlayerTaskException();

			PlayerTasks.Add( _newTask );
		}

		public void RemovePlayerTask( PlayerTask _playerTask )
		{
			if( _playerTask == null )
				throw new NullPlayerTaskException();

			PlayerTasks.Remove( _playerTask );
		}


		public string Name
		{
			get
			{
				return m_name;
			}

			set
			{
				if( value == null )
					throw new NullNameException();

				m_name = value;
			}
		}

		public int MinimumPlayers
		{
			get
			{
				return m_minimumPlayers;
			}
			set
			{
				if( value < 1 )
                    throw new MinimumNumberLessThanOneException();

				m_minimumPlayers = value;
			}
		}

		public int MaximumPlayers
		{
			get
			{
				return m_maximumPlayers;
			}
			set
			{
				if( value < MinimumPlayers )
					throw new MaximumNumberLessThanMinimumException();

				m_maximumPlayers = value;
			}
		}

		public virtual ICollection < PlayerTask > PlayerTasks { get; private set; }


		private string m_name;
		private int m_minimumPlayers;
		private int m_maximumPlayers;


		#region Entity Framework

		protected Scenario()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}