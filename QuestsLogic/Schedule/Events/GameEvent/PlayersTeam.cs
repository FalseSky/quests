﻿using System.Collections.Generic;
using QuestsLogic.Accounts;
using QuestsLogic.Schedule.Events.GameEvent.PlayerTeamExceptions;

namespace QuestsLogic.Schedule.Events.GameEvent
{
	public class PlayersTeam
	{
		public PlayersTeam( string _name )
		{
			Name = _name;

			Players = new List < PlayerAccount >();
		}


		public void AddPlayer( PlayerAccount _playerAccount )
		{
			if ( _playerAccount == null )
				throw new NullPlayerException();

			if ( Players.Contains( _playerAccount ) )
				throw new PlayerInTeamException();

			if ( Players.Count == 10 )
				throw new FullTeamException();

			if (_playerAccount.PlayersTeam != null)
				throw new PlayerInTeamException();

			Players.Add( _playerAccount );
		}

		public void RemovePlayer( PlayerAccount _playerAccount )
		{
			if( ! Players.Contains( _playerAccount ) )
				throw new PlayerNotExistException();

			Players.Remove( _playerAccount );
		}


		public void OnGameEventEnded( GameEvent _gameEvent )
		{
			foreach ( PlayerAccount player in Players )
				player.OnGameEventEnded( _gameEvent );
		}


		public string Name { get; private set; }

		public virtual ICollection < PlayerAccount > Players { get; private set; }
		
		
		#region Entity Framework

		protected PlayersTeam()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}