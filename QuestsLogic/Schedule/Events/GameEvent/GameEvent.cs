﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestsLogic.Accounts;
using QuestsLogic.Scenarios;
using QuestsLogic.Schedule.Events.EventExceptions;
using QuestsLogic.Schedule.Events.GameEvent.GameEventExceptions;

namespace QuestsLogic.Schedule.Events.GameEvent
{
	public class GameEvent : BaseEvent
	{
		public GameEvent( DateTime _time, string _name, string _description, decimal _price, Scenario _scenario )
			:	base( _time, _name, _description, _price )
		{
			GameScenario = _scenario;

			TeamProgress = new HashSet < TeamProgress >();
		}


		public override void End()
		{
			base.End();

			foreach ( TeamProgress teamProgress in TeamProgress )
			{
				teamProgress.PlayersTeam.OnGameEventEnded( this );
			}
		}


		public void AddTeam( PlayersTeam _playersTeam )
		{
			if( _playersTeam == null )
				throw new NullPlayerTeamException();

			if( TeamProgress.FirstOrDefault( _teamProgress => _teamProgress.PlayersTeam == _playersTeam ) != null )
				throw new TeamExistException();

			if( State != EventState.NotStarted )
				throw new EventStartedException();

			TeamProgress.Add( new TeamProgress( _playersTeam, GameScenario ) );
		}

		public void RemoveTeam( PlayersTeam _playersTeam )
		{
			if( _playersTeam == null )
				throw new NullPlayerTeamException();

			if( State != EventState.NotStarted )
				throw new EventStartedException();

			DeleteTeam( _playersTeam );
		}

		public void DisqualifyTeam( PlayersTeam _playersTeam, JudgeAccount _judgeAccount, object _judgeVerificatorKey )
		{
			_judgeAccount.Verificator.ThrowIfKeyInvalid( _judgeVerificatorKey );

			DeleteTeam( _playersTeam );
		}


		public virtual ICollection < TeamProgress > TeamProgress { get; private set; }

		public virtual Scenario GameScenario { get; private set; }


		private void DeleteTeam( PlayersTeam _playersTeam )
		{
			if( TeamProgress.First( _teamProgress => _teamProgress.PlayersTeam == _playersTeam ) == null )
				throw new TeamNotExistsException();

			foreach ( TeamProgress teamProgress in TeamProgress.Where( _teamProgress => _teamProgress.PlayersTeam == _playersTeam ) )
			{
				TeamProgress.Remove( teamProgress );
			}
		}
		

		#region Entity Framework

		protected GameEvent()
		{
			
		}

		#endregion
	}
}