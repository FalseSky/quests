﻿using System.Linq;
using QuestsLogic.Scenarios;
using QuestsLogic.Scenarios.Tasks;

namespace QuestsLogic.Schedule.Events.GameEvent
{
	public class TeamProgress
	{
		public TeamProgress( PlayersTeam _playersTeam, Scenario _scenario )
		{
			CurrentTeamTaskIndex = 0;

			PlayersTeam = _playersTeam;
			Scenario = _scenario;
		}


		public bool CompleteTask( object _currentTaskKey )
		{
			PlayerTask playerTask = GetCurrentTask();
			if ( playerTask != null )
			{
				if ( ! playerTask.CompleteTaskVerificator.ValidKey( _currentTaskKey ) )
					return false;

				++CurrentTeamTaskIndex;
			}

			return true;
		}

		public string CurrentTaskDescription
		{
			get
			{
				PlayerTask playerTask = GetCurrentTask();

				return playerTask ?. Description;
			}
		}


		private PlayerTask GetCurrentTask()
		{
			return Scenario.PlayerTasks.ElementAtOrDefault( CurrentTeamTaskIndex );
		}


		public virtual PlayersTeam PlayersTeam { get; private set; }

		public virtual Scenario Scenario { get; private set; }

		public int CurrentTeamTaskIndex { get; private set; }


		#region Entity Framework

		protected TeamProgress()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}