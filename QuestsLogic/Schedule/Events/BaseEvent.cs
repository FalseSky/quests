﻿using System;
using System.Collections.Generic;
using QuestsLogic.Accounts;
using QuestsLogic.Schedule.Events.EventExceptions;
using QuestsLogic.Tools;

namespace QuestsLogic.Schedule.Events
{
	public abstract class BaseEvent
	{
		protected BaseEvent( DateTime _time, string _name, string _description, decimal _price = 0 )
		{
			Time = _time;

			Name = _name;
			Description = _description;
			Price = _price;

			State = EventState.NotStarted;
			Comments = new List < Message >();
		}


		public virtual void Start()
		{
			if( State == EventState.Ended )
				throw new EventEndedException();
			
			if( State == EventState.Started )
				throw new EventStartedException();

			State = EventState.Started;
		}

		public virtual void Pause()
		{
			if( State == EventState.NotStarted )
				throw new EventNotStartedException();

			if( State == EventState.Paused )
				throw new EventPausedException();

			if( State == EventState.Ended )
				throw new EventEndedException();

			State = EventState.Paused;
		}
		
		public virtual void End()
		{
			if( State == EventState.NotStarted )
				throw new EventNotStartedException();

			if( State == EventState.Ended )
				throw new EventEndedException();

			State = EventState.Ended;
		}


		public void AddComment( UserAccount _user, string _message )
		{
			Comments.Add( new Message( _user, _message ) );
		}

		public void RemoveComment( Message _comment )
		{
			Comments.Remove( _comment );
		}


		public string Name { set; get; }

		public string Description { set; get; }

		public decimal Price
		{
			get
			{
				return m_price;
			}

			set
			{
				if( m_price < 0 )
					throw new NegativePriceException();

				m_price = value;
			}
		}

		public EventState State { get; private set; }

		public DateTime Time { get; private set; }

		public virtual ICollection < Message > Comments { get; private set; }

		
		private decimal m_price;


		#region Entity Framework

		protected BaseEvent()
		{
			
		}

		public int Id { get; set; }

		#endregion
	}
}