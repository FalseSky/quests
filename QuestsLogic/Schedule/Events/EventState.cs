﻿namespace QuestsLogic.Schedule.Events
{
	public enum EventState
	{
		NotStarted,
		Started,
		Paused,
		Ended
	}
}