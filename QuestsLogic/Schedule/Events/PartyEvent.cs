﻿using System;

namespace QuestsLogic.Schedule.Events
{
	public class PartyEvent : BaseEvent
	{
		public PartyEvent( DateTime _time, string _name, string _description )
			:	base( _time, _name, _description )
		{

		}

		#region Entity Framework

		protected PartyEvent()
		{
			
		}

		#endregion
	}
}