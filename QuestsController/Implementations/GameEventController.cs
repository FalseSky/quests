﻿using System.Linq;
using QuestsController.Implementations.GameEventControllerExceptions;
using QuestsController.Interfaces;
using QuestsLogic.Accounts;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsOrm;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class GameEventController : EventController, IGameEventController
	{
		public GameEventController()
		{
			m_userAccountRepository = RepositoryFactory.MakeUserAccountsRepository( GetDatabaseContext() );
		}


		public void AddTeam( int _gameEventId, int _playersTeamId )
		{
			PlayersTeam playersTeam = GetPlayerTeamOrDefault( _playersTeamId );

			GetGameEventById( _gameEventId ).AddTeam( playersTeam );

			m_eventRepository.Commit();
		}

		public void RemoveTeam( int _gameEventId, int _playersTeamId )
		{
			PlayersTeam playersTeam = GetPlayerTeamOrDefault( _playersTeamId );

			GetGameEventById( _gameEventId ).RemoveTeam( playersTeam );

			m_eventRepository.Commit();
		}

		public void DisqualifyTeam( int _gameEventId, int _playersTeamId, int _judgeAccountId, object _judgeVerificatorKey )
		{
			PlayersTeam playersTeam = GetPlayerTeamOrDefault( _playersTeamId );
			JudgeAccount judgeAccount = m_userAccountRepository.Load( _judgeAccountId ) as JudgeAccount;

			GetGameEventById( _gameEventId ).DisqualifyTeam( playersTeam, judgeAccount, _judgeVerificatorKey );

			m_eventRepository.Commit();
		}


		private GameEvent GetGameEventById( int _gameEventId )
		{
			GameEvent gameEvent = m_eventRepository.Load( _gameEventId ) as GameEvent;
			if ( gameEvent == null )
				throw new NotGameEventIdException();

			return gameEvent;
		}

		private PlayersTeam GetPlayerTeamOrDefault( int _playersTeamId )
		{
			IQueryable < UserAccount > userAccounts = m_userAccountRepository.LoadAll();
			foreach ( UserAccount userAccount in userAccounts )
			{
				PlayerAccount playerAccount = userAccount as PlayerAccount;

				if ( playerAccount == null )
					continue;

				if( playerAccount.PlayersTeam == null )
					continue;
				
				if( playerAccount.Id != _playersTeamId )
					continue;

				return playerAccount.PlayersTeam;
			}

			return null;
		}


		protected IUserAccountsRepository m_userAccountRepository;
	}
}