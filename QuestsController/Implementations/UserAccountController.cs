﻿using System.Collections.Generic;
using System.Linq;
using QuestsController.Interfaces;
using QuestsController.ViewModels;
using QuestsLogic.Accounts;
using QuestsLogic.Tools;
using QuestsOrm;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class UserAccountController : BasicController, IUserAccountController
	{
		public UserAccountController()
		{
			m_userAccountsRepository = RepositoryFactory.MakeUserAccountsRepository( GetDatabaseContext() );
		}


		public void SetName( int _userId, object _userKey, string _newName )
		{
			UserAccount userAccount = m_userAccountsRepository.Load( _userId );

			using ( IAccountManagerController accountManagerController = ControllerFactory.MakeAccountManagerController() )
			{
				accountManagerController.RenameAccount( userAccount.Name, _userKey, _newName );
			}

			m_userAccountsRepository.Commit();
		}

		public void SetVerificator( int _userId, object _userKey, object _newVerificator )
		{
			m_userAccountsRepository.Load( _userId ).SetVerificator( _userKey, new Verifier( _newVerificator ) );

			m_userAccountsRepository.Commit();
		}

		public void AddMessage( int _userId, int _senderId, string _messageText )
		{
			UserAccount userAccount = m_userAccountsRepository.Load( _userId );

			m_userAccountsRepository.Load( _userId ).AddMessage( userAccount, _messageText );

			m_userAccountsRepository.Commit();
		}

		public void RemoveMessage( int _userId, object _userKey, int _messageId )
		{
			UserAccount userAccount = m_userAccountsRepository.Load( _userId );
			Message message = userAccount.UserMessage.FirstOrDefault( _message => _message.Id == _messageId );

			userAccount.RemoveMessage( _userKey, message );

			m_userAccountsRepository.Commit();
		}


		public ICollection < UserAccountView > GetAllUserAccount()
		{
			return ViewModelsFactory.BuildViewModels( m_userAccountsRepository.LoadAll().AsQueryable(), ViewModelsFactory.BuildUserAccountView );
		}


		protected readonly IUserAccountsRepository m_userAccountsRepository;
	}
}