﻿using QuestsController.Implementations.ScenariosBaseExceptions;
using QuestsController.Interfaces;
using QuestsController.ViewModels;
using QuestsLogic.Scenarios;
using QuestsOrm.Implementations;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class ScenariosBaseController : BasicController, IScenariosBaseController
	{
		public ScenariosBaseController()
		{
			m_userAccountsRepository = new ScenariosRepository( GetDatabaseContext() );
		}


		public void AddScenario( string _newScenario )
		{

		    var scenario = new Scenario( _newScenario );
			if( _newScenario == null )
				throw new NullScenarioException();

			ThrowIfScenarioNameIsBusy( scenario.Name );

			m_userAccountsRepository.Add(scenario);

			m_userAccountsRepository.Commit();
		}
		
		public void RenameScenario( string _oldScenarioName, string _newScenarioName )
		{
			ThrowIfScenarioNotExist( _oldScenarioName );

			ThrowIfScenarioNameIsBusy( _newScenarioName );

			Scenario scenario = m_userAccountsRepository.FindByName( _oldScenarioName );

			scenario.Name = _newScenarioName;

			m_userAccountsRepository.Commit();
		}
		
		public void RemoveScenario( string _scenarioName )
		{
			ThrowIfScenarioNotExist( _scenarioName );

			m_userAccountsRepository.RemoveByName( _scenarioName );

			m_userAccountsRepository.Commit();
		}


		public bool Contain( string _scenarioName )
		{
			return m_userAccountsRepository.FindByName( _scenarioName ) != null;
		}

		public ScenarioView Get( string _scenarioName )
		{
			ThrowIfScenarioNotExist( _scenarioName );

			return ViewModelsFactory.BuildScanarioView(m_userAccountsRepository.FindByName( _scenarioName ));
		}


		private void ThrowIfScenarioNameIsBusy( string _scenarioName )
		{
			if ( Contain( _scenarioName ) )
				throw new BusyScenarioNameException();
		}

		private void ThrowIfScenarioNotExist( string _scenarioName )
		{
			if ( ! Contain( _scenarioName ) )
				throw new ScenarioNotExistException();
		}


		private readonly IScenariosRepository m_userAccountsRepository;
	}
}