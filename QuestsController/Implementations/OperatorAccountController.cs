﻿using QuestsController.Implementations.OperatorAccountControllerExceptions;
using QuestsController.Interfaces;
using QuestsController.ViewModels;
using QuestsLogic.Accounts;
using QuestsLogic.Tools;

namespace QuestsController.Implementations
{
	public class OperatorAccountController : UserAccountController, IOperatorAccountController
	{
		public void NewProblemReport( int _userId, int _userAccountId, string _problemReport )
		{
			GetOperatorAccountById( _userId ).NewProblemReport( m_userAccountsRepository.Load( _userAccountId ), _problemReport );

			m_userAccountsRepository.Commit();
		}

		public void NewPayment( int _userId, int _playerAccountId, string _paymentDestination, decimal _paymentSize )
		{
			GetOperatorAccountById( _userId ).NewPayment( m_userAccountsRepository.Load( _playerAccountId ) as PlayerAccount, _paymentDestination, _paymentSize );

			m_userAccountsRepository.Commit();
		}


		public ProblemReportView PopProblemReport( int _userId, object _accountKey )
		{
			ProblemReport problemReport = GetOperatorAccountById( _userId ).PopProblemReport( _accountKey );

			m_userAccountsRepository.Commit();

			return ViewModelsFactory.BuildProblemReportView( problemReport );
		}

		public PaymentReportView PopPaymentReport( int _userId, object _accountKey )
		{
			PaymentReport paymentReport = GetOperatorAccountById( _userId ).PopPaymentReport( _accountKey );

			m_userAccountsRepository.Commit();

			return ViewModelsFactory.BuildPaymentReportView( paymentReport );
		}


		private OperatorAccount GetOperatorAccountById( int _operatorAccountId )
		{
			OperatorAccount operatorAccount = m_userAccountsRepository.Load( _operatorAccountId ) as OperatorAccount;
			if( operatorAccount == null )
				throw new NotOperatorAccountIdException();

			return operatorAccount;
		}
	}
}