﻿using System.Collections.Generic;
using QuestsController.Interfaces;
using QuestsLogic.Scenarios;
using QuestsLogic.Scenarios.Tasks;

using QuestsOrm.Implementations;
using QuestsOrm.Interfaces;
using System.Linq;
using QuestsController.ViewModels;
using QuestsLogic.Tools;

namespace QuestsController.Implementations
{
    public class ScenarioController : BasicController, IScenarioController

    {
        public ScenarioController()
        {
            m_scenarioRepository = new ScenariosRepository(GetDatabaseContext());
        }

        public void RenameScenario(string _scenarioName, string _newScenarioName)
        {
            m_scenarioRepository.FindByName(_scenarioName).Name = _newScenarioName;
            m_scenarioRepository.Commit();
        }

        public void SetMaximumPlayers(string _scenarioName, int _maximumPlayers)
        {
            m_scenarioRepository.FindByName(_scenarioName).MaximumPlayers = _maximumPlayers;
            m_scenarioRepository.Commit();
        }

        public void SetMinimumPlayers(string _scenarioName, int _minimumPlayers)
        {
            m_scenarioRepository.FindByName(_scenarioName).MinimumPlayers=_minimumPlayers;
            m_scenarioRepository.Commit();
        }

        public void AddPlayerTask(string _scenarioName, string _taskDescription, object _verificator)
        {
            var task = new PlayerTask(_taskDescription, new Verifier(_verificator));
            m_scenarioRepository.FindByName(_scenarioName).PlayerTasks.Add(task);
            m_scenarioRepository.Commit();
        }

        public void RemovePlayerTask(string _scenarioName, int _playerTaskId)
        {
            m_scenarioRepository.FindByName(_scenarioName).RemovePlayerTask(getScenarioById( _scenarioName,_playerTaskId ));
            m_scenarioRepository.Commit();
        }

        public ICollection <ScenarioView> GetAllScenarios()
        {
            return ViewModelsFactory.BuildViewModels( m_scenarioRepository.LoadAll().AsQueryable(),
                ViewModelsFactory.BuildScanarioView );
        }

        private PlayerTask getScenarioById(string _scenario, int _playerTaskId)
        {
            PlayerTask task = null;
            Scenario scenario = null;
            scenario = m_scenarioRepository.FindByName( _scenario );
            foreach ( var item in scenario.PlayerTasks)
            {
                if ( item.Id == _playerTaskId )
                     task=item;
                break;
            }
            return task;
        }

        protected readonly IScenariosRepository m_scenarioRepository;
    }
}
