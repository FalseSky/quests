﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestsController.Implementations.ScheduleControllerExceptions;
using QuestsController.Interfaces;
using QuestsController.ViewModels;
using QuestsLogic.Schedule.Events;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsOrm;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class ScheduleController : BasicController, IScheduleController
	{
		public ScheduleController()
		{
			m_eventRepository = RepositoryFactory.MakeEventRepository( GetDatabaseContext() );
			m_scenariosRepository = RepositoryFactory.MakeScenariosRepository( GetDatabaseContext() );
		}


		public void AddGameEvent( DateTime _time, string _name, string _description, decimal _price, int _scenarioId )
		{
		    var Event = new GameEvent( _time, _name, _description, _price, m_scenariosRepository.Load( _scenarioId ) ); 
			if(Event == null )
				throw new NullEventException();

			m_eventRepository.Add(Event);

			m_eventRepository.Commit();
		}

	    public void AddPartyEvent(DateTime _time, string _name, string _description)
        { 
            m_eventRepository.Add(new PartyEvent(_time, _name, _description));

            m_eventRepository.Commit();
        }

		public void Remove( int _eventId )
		{
		    m_eventRepository.RemoveEvent( m_eventRepository.Load( _eventId ) );

			m_eventRepository.Commit();
		}


		public ICollection< EventView > GetAllEvents()
		{
			return ViewModelsFactory.BuildViewModels( m_eventRepository.LoadAll().AsQueryable(), ViewModelsFactory.BuildEventView );
		}

	    private readonly IScenariosRepository m_scenariosRepository;
		private readonly IEventRepository m_eventRepository;
	}
}