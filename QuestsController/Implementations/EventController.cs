﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using QuestsController.Interfaces;
using QuestsController.ViewModels;
using QuestsLogic.Accounts;
using QuestsLogic.Schedule.Events;
using QuestsLogic.Tools;
using QuestsOrm;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class EventController : BasicController, IEventController
	{
		public EventController()
		{
			m_eventRepository = RepositoryFactory.MakeEventRepository( GetDatabaseContext() );
			m_userAccountsRepository = RepositoryFactory.MakeUserAccountsRepository( GetDatabaseContext() );
		}


		public void Start( int _eventId )
		{
			m_eventRepository.Load( _eventId ).Start();

			m_eventRepository.Commit();
		}

		public void Pause( int _eventId )
		{
			m_eventRepository.Load( _eventId ).Pause();

			m_eventRepository.Commit();
		}

		public void End( int _eventId )
		{
			m_eventRepository.Load( _eventId ).End();

			m_eventRepository.Commit();
		}


		public void AddComment( int _eventId, int _userId, string _message )
		{
			UserAccount userAccount = m_userAccountsRepository.Load( _userId );
			m_eventRepository.Load( _eventId ).AddComment( userAccount, _message );

            m_eventRepository.Commit();
		}

		public void RemoveComment( int _eventId, int _commentId )
		{
			BaseEvent baseEvent = m_eventRepository.Load( _eventId );
			Message message = baseEvent.Comments.FirstOrDefault( _comment => _comment.Id == _commentId );

			baseEvent.RemoveComment( message );

            m_eventRepository.Commit();
		}


		public void SetName( int _eventId, string _newName )
		{
			m_eventRepository.Load( _eventId ).Name = _newName;

            m_eventRepository.Commit();
		}

		public void SetDescription( int _eventId, string _newDescription )
		{
			m_eventRepository.Load( _eventId ).Description = _newDescription;

            m_eventRepository.Commit();
		}

		public void SetPrice( int _eventId, decimal _newPrice )
		{
			m_eventRepository.Load( _eventId ).Price = _newPrice;

            m_eventRepository.Commit();
		}


		public ICollection< EventView > GetAllEvents()
		{
			return ViewModelsFactory.BuildViewModels( m_eventRepository.LoadAll().AsQueryable(), ViewModelsFactory.BuildEventView );
		}


		protected readonly IEventRepository m_eventRepository;

		protected readonly IUserAccountsRepository m_userAccountsRepository;
	}
}