﻿using QuestsOrm;
using QuestsOrm.Interfaces;
using QuestsLogic.Accounts;
using QuestsController.Interfaces;
using QuestsController.Implementations.AccountManagerExceptions;
using QuestsController.ViewModels;
using QuestsLogic.Tools;

namespace QuestsController.Implementations
{
	public class AccountManagerController : BasicController, IAccountManagerController
	{
		public AccountManagerController()
		{
			m_userAccountsRepository = RepositoryFactory.MakeUserAccountsRepository( GetDatabaseContext() );
		}


		public void RegisterNewPlayer( string _playerName, object _playerVerificator )
		{
            
			RegisterNewUser( _playerName, new Verifier( _playerVerificator ), UserType.Player );
		}

		public void RegisterNewAccount( string _accountName, object _accountVerificator, UserType _userType )
		{
			RegisterNewUser( _accountName, new Verifier( _accountVerificator ), _userType );
		}
		
		public void RenameAccount( string _oldAccountName, object _accountKey, string _newAccountName )
		{
			ThrowIfAccountNameIsBusy( _newAccountName );
			ThrowIfAccountNotExist( _oldAccountName );

			UserAccount userAccount = m_userAccountsRepository.FindByName( _oldAccountName );
			userAccount.SetName( _accountKey, _newAccountName );

			m_userAccountsRepository.Commit();
		}
		
		public void RemoveAccount( string _accountName, object _accountKey )
		{
			ThrowIfAccountNotExist( _accountName );

			m_userAccountsRepository.FindByName( _accountName ).Verificator.ThrowIfKeyInvalid( _accountKey );

			m_userAccountsRepository.RemoveByName( _accountName );

			m_userAccountsRepository.Commit();
		}


		public PlayerAccountView EnterAsPlayer( string _userName, object _accountKey )
		{
			return ViewModelsFactory.BuilPlayerAccountView( GetUserAccount< PlayerAccount >( _userName, _accountKey ));
		}

		public OperatorAccountView EnterAsOperator( string _userName, object _accountKey )
		{
			return ViewModelsFactory.BuildOperatorAccountView( GetUserAccount< OperatorAccount >( _userName, _accountKey ) );
		}

		public UserAccountView EnterAsJudge( string _userName, object _accountKey )
		{
			return ViewModelsFactory.BuildUserAccountView( GetUserAccount< JudgeAccount >( _userName, _accountKey ) );
		}

		public UserAccountView EnterAsScreenwriter( string _userName, object _accountKey )
		{
			return ViewModelsFactory.BuildUserAccountView( GetUserAccount< ScreenwriterAccount >( _userName, _accountKey ) );
		}

		public UserAccountView EnterAsMaster( string _userName, object _accountKey )
		{
			return ViewModelsFactory.BuildUserAccountView( GetUserAccount< MasterAccount >( _userName, _accountKey ) );
		}


		private void RegisterNewUser( string _accountName, object _accountVerificator, UserType _userType )
		{
			if( string.IsNullOrEmpty( _accountName ) )
				throw new NullAccountNameException();

			if ( _accountVerificator == null )
				throw new NullAccountVerificatorException();

			ThrowIfAccountNameIsBusy( _accountName );

			UserAccount newUserAccount;

			switch ( _userType )
			{
				case UserType.Player:
					newUserAccount = new PlayerAccount( _accountName, new Verifier( _accountVerificator ) );
					break;
				case UserType.Operator:
					newUserAccount = new OperatorAccount( _accountName, new Verifier(_accountVerificator));
					break;
				case UserType.Judge:
					newUserAccount = new JudgeAccount( _accountName, new Verifier(_accountVerificator));
					break;
				case UserType.Screenwriter:
					newUserAccount = new ScreenwriterAccount( _accountName, new Verifier(_accountVerificator));
					break;
				case UserType.Master:
					newUserAccount = new MasterAccount( _accountName, new Verifier(_accountVerificator));
					break;

				default:
					throw new InvalidAccountTypeException();		
			}

			m_userAccountsRepository.Add( newUserAccount );

			m_userAccountsRepository.Commit();
		}

		private TAccountType GetUserAccount < TAccountType > ( string _userName, object _accountKey ) where TAccountType : UserAccount
		{
			ThrowIfAccountNotExist( _userName );

			TAccountType requestedAccount = m_userAccountsRepository.FindByName( _userName ) as TAccountType;
			if ( requestedAccount == null )
				throw new InvalidAccountTypeException();

			requestedAccount.Verificator.ThrowIfKeyInvalid( _accountKey );

			return requestedAccount;
		}


		private void ThrowIfAccountNameIsBusy( string _accountName )
		{
			if( m_userAccountsRepository.FindByName( _accountName ) != null )
				throw new BusyAccountNameException();
		}

		private void ThrowIfAccountNotExist( string _accountName )
		{
			if( m_userAccountsRepository.FindByName( _accountName ) == null )
				throw new AccountNotExistException();
		}


		private readonly IUserAccountsRepository m_userAccountsRepository;
	}
}