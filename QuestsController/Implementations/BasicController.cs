﻿using System;
using QuestsOrm;

namespace QuestsController.Implementations
{
	public class BasicController
	{
		protected QuestsDatabaseFacade GetDatabaseContext()
		{
			return m_databaseContext ?? ( m_databaseContext = new QuestsDatabaseFacade() );
		}


		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		protected virtual void Dispose ( bool _disposing )
        {
            if ( _disposing )
                m_databaseContext ?. Dispose();
        }


		private QuestsDatabaseFacade m_databaseContext;
	}
}