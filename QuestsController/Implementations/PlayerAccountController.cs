﻿using System.Linq;
using QuestsController.Implementations.PlayerAccountControllerExceptions;
using QuestsController.Interfaces;
using QuestsLogic.Accounts;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsLogic.Tools;
using QuestsOrm;
using QuestsOrm.Interfaces;

namespace QuestsController.Implementations
{
	public class PlayerAccountController : UserAccountController, IPlayerAccountController
	{
		public PlayerAccountController()
		{
			m_eventRepository = RepositoryFactory.MakeEventRepository( GetDatabaseContext() );
		}

		public void OnPaymentAccepted( int _playerId, int _playerAccountId, string _paymentDestination, decimal _paymentSize )
		{
			GetPlayerAccountById( _playerId ).OnPaymentAccepted(
				new PaymentReport(
					m_userAccountsRepository.Load( _playerId ) as PlayerAccount
					, _paymentDestination
					, _playerAccountId
				)
			);

			m_userAccountsRepository.Commit();
		}

		public void OnGameEventEnded( int _playerId, int _gameEventId )
		{
			GetPlayerAccountById( _playerId ).OnGameEventEnded( m_eventRepository.Load( _gameEventId ) as GameEvent );

			m_userAccountsRepository.Commit();
		}

		public void OnPlayerJoinToTeam( int _playerId, int _playersTeamId )
		{
			IQueryable < UserAccount > userAccounts = m_userAccountsRepository.LoadAll();
			foreach ( UserAccount userAccount in userAccounts )
			{
				PlayerAccount playerAccount = userAccount as PlayerAccount;

				if ( playerAccount == null )
					continue;

				if( playerAccount.PlayersTeam == null )
					continue;
				
				if( playerAccount.Id != _playersTeamId )
					continue;

				GetPlayerAccountById( _playerId ).OnPlayerJoinToTeam( playerAccount.PlayersTeam );
				m_userAccountsRepository.Commit();
				break;
			}
		}


		public void AddRating( int _playerId, int _rating )
		{
			GetPlayerAccountById( _playerId ).PlayerRating += _rating;

			m_userAccountsRepository.Commit();
		}

		public void RemoveRating( int _playerId, int _rating )
		{
			GetPlayerAccountById( _playerId ).PlayerRating -= _rating;

			m_userAccountsRepository.Commit();
		}


		public void AddTotalDiscount( int _playerId, int _discount )
		{
			GetPlayerAccountById( _playerId ).TotalDiscount += _discount;

			m_userAccountsRepository.Commit();
		}

		public void RemoveTotalDiscount( int _playerId, int _discount )
		{
			GetPlayerAccountById( _playerId ).TotalDiscount -= _discount;

			m_userAccountsRepository.Commit();
		}

		
		private PlayerAccount GetPlayerAccountById( int _playerAccountId )
		{
			PlayerAccount playerAccount = m_userAccountsRepository.Load( _playerAccountId ) as PlayerAccount;
			if( playerAccount == null )
				throw new NotPlayerAccountIdException();

			return playerAccount;
		}


		private IEventRepository m_eventRepository;
	}
}