﻿namespace QuestsController.Implementations
{
	public enum UserType
	{
			Player
		,	Operator
		,	Judge
		,	Screenwriter
		,	Master
	}
}