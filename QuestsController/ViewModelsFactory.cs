﻿using System;
using System.Collections.Generic;
using System.Linq;
using QuestsController.ViewModels;
using QuestsLogic.Accounts;
using QuestsLogic.Scenarios;
using QuestsLogic.Scenarios.Tasks;
using QuestsLogic.Schedule.Events;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsLogic.Tools;

namespace QuestsController
{
	static class ViewModelsFactory
	{
		static internal UserAccountView BuildUserAccountView ( UserAccount _userAccount )
        {
            return new UserAccountView
            {
                Id = _userAccount.Id,
                Name = _userAccount.Name,
                Verificator = BuildVerifierView( _userAccount.Verificator ),
                UserMessage = BuildViewModels( _userAccount.UserMessage.AsQueryable(), BuildMessageView )
            };
        }

		static internal VerifierView BuildVerifierView ( Verifier _verifier )
        {
            return new VerifierView
            {
				Id = _verifier.Id,
				KeyHash = _verifier.KeyHash
            };
        }

		static internal PaymentReportView BuildPaymentReportView ( PaymentReport _paymentReport )
        {
            return new PaymentReportView
            {
				Id = _paymentReport.Id,
				Destination = _paymentReport.Destination,
				UserName = _paymentReport.UserName,
				Size = _paymentReport.Size
            };
        }

		static internal ProblemReportView BuildProblemReportView ( ProblemReport _problemReport )
        {
            return new ProblemReportView
            {
				Id = _problemReport.Id,
				UserName = _problemReport.UserName,
				ProblemDescription = _problemReport.ProblemDescription
            };
        }

		static internal OperatorAccountView BuildOperatorAccountView ( OperatorAccount _operatorAccount )
        {
            return new OperatorAccountView
            {
                Id = _operatorAccount.Id,
                Name = _operatorAccount.Name,
                Verificator = BuildVerifierView( _operatorAccount.Verificator ),
                UserMessage = BuildViewModels( _operatorAccount.UserMessage.AsQueryable(), BuildMessageView ),
				ProblemReports = BuildViewModels( _operatorAccount.ProblemReports.AsQueryable(), BuildProblemReportView ),
				PaymentReports = BuildViewModels( _operatorAccount.PaymentReports.AsQueryable(), BuildPaymentReportView )
            };
        }

	    static internal EventView BuildEventView( BaseEvent _event )
	    {
	        return new EventView
	        {
	            Name = _event.Name,
                Comments = BuildViewModels( _event.Comments.AsQueryable(), BuildMessageView),
                Description = _event.Description,
                Price = _event.Price,
                State = _event.State,
                Time = _event.Time
            };
	    }

	    static internal MessageView BuildMessageView( Message _message )
	    {
	        return new MessageView
	        {
	            Id = _message.Id,
	            MessageText = _message.MessageText,
	            Time = _message.Time,
	            UserName = _message.UserName
	        };
	    }

        static internal ScenarioView BuildScanarioView(Scenario _scenario)
        {
            return new ScenarioView
            {
                MaximumPlayers = _scenario.MaximumPlayers,
                MinimumPlayers = _scenario.MinimumPlayers,
                PlayerTasks = BuildViewModels(_scenario.PlayerTasks.AsQueryable(), BuilPlayerTaskView),
                ScenarioName = _scenario.Name
            };
        }

	    static internal PlayerTaskView BuilPlayerTaskView( PlayerTask _task )
	    {
	        return new PlayerTaskView
	        {
	            Id = _task.Id,
	            CompleteTaskVerificator = BuildVerifierView( _task.CompleteTaskVerificator ),
	            Description = _task.Description
	        };
	    }

	    static internal GameEventView BuildGameEventView( GameEvent _gameEvent )
	    {
	        return new GameEventView
	        {
	            Comments = BuildViewModels( _gameEvent.Comments.AsQueryable(), BuildMessageView ),
	            Description = _gameEvent.Description,
	            GameScenario = BuildScanarioView( _gameEvent.GameScenario ),
	            Name = _gameEvent.Name,
	            Price = _gameEvent.Price,
	            State = _gameEvent.State,
	            TeamProgress = BuildViewModels( _gameEvent.TeamProgress.AsQueryable(), BuilTeamProgressView),
	            Time = _gameEvent.Time

	        };
	    }

	    static internal TeamProgressView BuilTeamProgressView( TeamProgress _teamProgress )
	    {
	        return new TeamProgressView
	        {
	            CurrentTeamTaskIndex = _teamProgress.CurrentTeamTaskIndex,
	            Scenario = BuildScanarioView( _teamProgress.Scenario )
	        };
	    }

	    static internal PlayerAccountView BuilPlayerAccountView( PlayerAccount _playerAccount )
	    {
	        return new PlayerAccountView
	        {
	            EventHistory = BuildViewModels( _playerAccount.EventHistory.AsQueryable(), BuildGameEventView ),
	            Id = _playerAccount.Id,
	            Name = _playerAccount.Name,
	            PaymentHistory = BuildViewModels( _playerAccount.PaymentHistory.AsQueryable(), BuildPaymentReportView),
	            PlayerRating = _playerAccount.PlayerRating,
	            UserMessage = BuildViewModels( _playerAccount.UserMessage.AsQueryable(), BuildMessageView ),
	            Verificator = BuildVerifierView( _playerAccount.Verificator )
	        };
	    }

        static internal ICollection<TView> BuildViewModels<TView, TEntity>(
            IQueryable<TEntity> query,
            Func<TEntity, TView> converter
        )
        {
            var results = new List<TView>();
            foreach (TEntity entity in query)
                results.Add(converter(entity));
            return results;
        }
	}
}