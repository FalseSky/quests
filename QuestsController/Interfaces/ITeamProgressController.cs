﻿using System;

namespace QuestsController.Interfaces
{
	public interface ITeamProgressController : IDisposable
	{
		bool CompleteTask( object _currentTaskKey );
	}
}