﻿using System;
using QuestsController.Implementations;
using QuestsController.ViewModels;
using QuestsLogic.Accounts;

namespace QuestsController.Interfaces
{
	public interface IAccountManagerController : IDisposable
	{
		void RegisterNewPlayer( string _playerName, object _playerVerificator );

		void RegisterNewAccount( string _accountName, object _accountVerificator, UserType _userType );


		void RenameAccount( string _oldAccountName, object _accountKey, string _newAccountName );


		void RemoveAccount( string _accountName, object _accountKey );

		#region Getters

		PlayerAccountView EnterAsPlayer( string _userName, object _accountKey );

		OperatorAccountView EnterAsOperator( string _userName, object _accountKey );

		UserAccountView EnterAsJudge( string _userName, object _accountKey );

		UserAccountView EnterAsScreenwriter( string _userName, object _accountKey );

		UserAccountView EnterAsMaster( string _userName, object _accountKey );

		#endregion
	}
}