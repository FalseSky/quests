﻿using QuestsController.ViewModels;

namespace QuestsController.Interfaces
{
	public interface IOperatorAccountController : IUserAccountController
	{
		void NewProblemReport( int _userId, int _userAccountId, string _problemReport );

		void NewPayment( int _userId, int _playerAccountId, string _paymentDestination, decimal _paymentSize );


		ProblemReportView PopProblemReport( int _userId, object _accountKey );

		PaymentReportView PopPaymentReport( int _userId, object _accountKey );
	}
}