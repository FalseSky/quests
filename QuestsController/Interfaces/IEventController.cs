﻿using System;
using System.Collections.Generic;
using QuestsController.ViewModels;

namespace QuestsController.Interfaces
{
	public interface IEventController : IDisposable
	{
		void Start( int _eventId );

		void Pause( int _eventId );

		void End( int _eventId );


		void AddComment( int _eventId, int _userId, string _message );

		void RemoveComment( int _eventId, int _commentId );


		void SetName( int _eventId, string _newName );

		void SetDescription( int _eventId, string _newDescription );

		void SetPrice( int _eventId, decimal _newPrice );


		#region Getters

		ICollection< EventView > GetAllEvents();

		#endregion
	}
}