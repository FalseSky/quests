﻿using System;
using System.Collections.Generic;
using QuestsController.ViewModels;

namespace QuestsController.Interfaces
{
	public interface IScheduleController : IDisposable
	{
        void AddGameEvent( DateTime _time, string _name, string _description, decimal _price, int _scenarioId );

	    void AddPartyEvent( DateTime _time, string _name, string _description );

        void Remove(int _eventId);


		#region Getters

		ICollection< EventView > GetAllEvents();

		#endregion
	}
}