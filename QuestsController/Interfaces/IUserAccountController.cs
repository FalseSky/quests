﻿using System;
using System.Collections.Generic;
using QuestsController.ViewModels;

namespace QuestsController.Interfaces
{
	public interface IUserAccountController : IDisposable
	{
		void SetName( int _userId, object _userKey, string _newName );

		void SetVerificator( int _userId, object _userKey, object _newVerificator );


		void AddMessage( int _userId, int _senderId, string _messageText );

		void RemoveMessage( int _userId, object _userKey, int _messageId );


		#region Getters

		ICollection < UserAccountView > GetAllUserAccount();

		#endregion
	}
}