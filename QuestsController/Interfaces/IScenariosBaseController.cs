﻿using System;
using QuestsController.ViewModels;
using QuestsLogic.Scenarios;

namespace QuestsController.Interfaces
{
	public interface IScenariosBaseController : IDisposable
	{
		void AddScenario( string _scenarioName );

		void RenameScenario( string _oldScenarioName, string _newScenarioName );

		void RemoveScenario( string _scenarioName );


		#region Getters

		bool Contain( string _scenarioName );

		ScenarioView Get( string _scenarioName );

		#endregion
	}
}