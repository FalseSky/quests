﻿namespace QuestsController.Interfaces
{
	public interface IPlayerAccountController : IUserAccountController
	{
		void OnPaymentAccepted( int _playerId, int _playerAccountId, string _paymentDestination, decimal _paymentSize );

		void OnGameEventEnded( int _playerId, int _gameEventId );

		void OnPlayerJoinToTeam( int _playerId, int _playersTeamId );


		void AddRating( int _playerId, int _rating );

		void RemoveRating( int _playerId, int _rating );


		void AddTotalDiscount( int _playerId, int _discount );

		void RemoveTotalDiscount( int _playerId, int _discount );
	}
}