﻿using System;
using System.Collections.Generic;
using QuestsController.ViewModels;


namespace QuestsController.Interfaces
{
	public interface IScenarioController : IDisposable
	{
		void RenameScenario(string _scenarioName, string _newScenarioName );

		void SetMaximumPlayers(string _scenarioName, int _maximumPlayers );
		
		void SetMinimumPlayers(string _scenarioName, int _minimumPlayers );

		void AddPlayerTask(string _scenarioName, string _taskDescription, object _verificator);

		void RemovePlayerTask(string _scenarioName, int _playerTaskId);

        #region Getters

        ICollection< ScenarioView > GetAllScenarios();

        #endregion
	}
}