﻿namespace QuestsController.Interfaces
{
	public interface IGameEventController : IEventController
	{
		void AddTeam( int _gameEventId, int _playersTeamId );

		void RemoveTeam( int _gameEventId, int _playersTeamId );


		void DisqualifyTeam( int _gameEventId, int _playersTeamId, int _judgeAccountId, object _judgeVerificatorKey );
	}
}