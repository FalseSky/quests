﻿using System;
using System.Collections.Generic;


namespace QuestsController.ViewModels
{
	public class MessageView:BasicViewModel <MessageView>
    {
        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { Id, UserName, Time, MessageText };
        }

        public override string ToString()
        {
            return string.Format( "Id= {0} Name= {1} Time= {2}\nText: {3}",
                Id,
                UserName,
                Time,
                MessageText );
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public DateTime Time { get; set; }
        public string MessageText { get; set; }
    }
}
