﻿using System.Collections.Generic;

namespace QuestsController.ViewModels
{
	public class VerifierView : BasicViewModel < VerifierView >
	{
		public int Id { set; get; }

		public int KeyHash { get; set; }


		protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
		{
			return new List < object >{ Id, KeyHash };
		}

		public override string ToString()
		{
			return $"Id = {Id}";
		}
	}
}