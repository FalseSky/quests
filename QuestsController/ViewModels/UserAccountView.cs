﻿using System.Collections.Generic;

namespace QuestsController.ViewModels
{
	public class UserAccountView : BasicViewModel < UserAccountView >
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public virtual VerifierView Verificator { get; set; }

		public virtual ICollection < MessageView > UserMessage { get; set; }


		protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
		{
			return new List < object > { Id, Name };;
		}

		public override string ToString()
		{
			return $"Id = {Id}\nName = {Name}\nUserMessage: {string.Join( "\n", UserMessage )}";
		}
	}
}