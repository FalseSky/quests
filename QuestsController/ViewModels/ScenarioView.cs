﻿using System.Collections.Generic;

namespace QuestsController.ViewModels
{
    public class ScenarioView : BasicViewModel< ScenarioView >
    {
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object> { ScenarioName, MaximumPlayers, MinimumPlayers };
            list.AddRange( PlayerTasks );
            return list;
        }

        public override string ToString()
        {
            return string.Format(
                "Name = {0}\nMinimum Players= {1}\nMaximum Players= {2}\nPlayer Tasks= {3}\n",
                ScenarioName,
                MinimumPlayers,
                MaximumPlayers,
                string.Join( ",", PlayerTasks ));
        }

        public string ScenarioName { get; set; }
        public int MinimumPlayers { get; set; }
        public int MaximumPlayers { get; set; }
        public ICollection < PlayerTaskView > PlayerTasks { get; set; }
    }
}
