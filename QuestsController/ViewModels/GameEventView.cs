﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QuestsLogic.Schedule.Events;

namespace QuestsController.ViewModels
{
    public class GameEventView:BasicViewModel <GameEventView>
    {
        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List < object > { Name, Price, Description, Time, State, GameScenario };
            list.AddRange( Comments );
            list.AddRange( TeamProgress );
            return list;
        }

        public override string ToString()
        {
            return string.Format( "Name= {0}\nPrice= {1}\nTime= {2}\nDescription= {3}\nState= {4}\nComments= {5}" +
                                  "\nTeamProgress= {6}\nGame Scenario= {7}",
                Name, Price, Time, Description, State,
                string.Join( ",", Comments ),
                string.Join( ",", TeamProgress ),
                GameScenario );
        }

        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public EventState State { get; set; }
        public ICollection<MessageView> Comments { get; set; }
        public virtual ICollection<TeamProgressView> TeamProgress { get; set; }
        public virtual ScenarioView GameScenario { get; set; }
    }
}
