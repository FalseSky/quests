﻿using System.Collections.Generic;


namespace QuestsController.ViewModels
{
    class ScheduleView:BasicViewModel <ScheduleView>
    {
        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object> ();
            list.AddRange(EventRepository);
            return list;
        }

        public override string ToString()
        {
            return string.Format(
                "Event= {0}\n,",
                string.Join( ",", EventRepository ));
        }

        public ICollection<EventView> EventRepository { get; set; }
    }
}
