﻿
using System.Collections.Generic;


namespace QuestsController.ViewModels
{
    public class PlayerTaskView:BasicViewModel <PlayerTaskView>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public VerifierView CompleteTaskVerificator { get; set; }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { Id, Description, CompleteTaskVerificator };
        }

        public override string ToString()
        {
            return string.Format( "Id= {0}\nDecription= {1}\nVerifier= {2}\n",
                Id,
                Description,
                CompleteTaskVerificator );
        }
    }
}
