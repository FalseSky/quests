﻿using System;
using System.Collections.Generic;
using QuestsLogic.Schedule.Events;

namespace QuestsController.ViewModels
{
    public class EventView:BasicViewModel <EventView>
    {
        public override string ToString()
        {
            return string.Format(
                "Name= {0}\nPrice= {1}\nTime= {2}\nDescription= {3}\nState= {4}\nComments= {5}\n",
                Name,
                Price,
                Time,
                Description,
                State,
                string.Join( ",", Comments ) );
        }

        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List < object > { Name, Price, Time, State, Description };
            list.AddRange( Comments );
            return list;
        }

        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public DateTime Time { get; set; }
        public EventState State { get; set; }
        public ICollection<MessageView> Comments { get; set; }
    }
}
