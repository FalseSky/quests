﻿
using System.Collections.Generic;


namespace QuestsController.ViewModels
{
    public class TeamProgressView:BasicViewModel <TeamProgressView>
    {
        protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
        {
            return new List < object > { Scenario, CurrentTeamTaskIndex };
        }

        public override string ToString()
        {
            return string.Format( "Scenario= {0}\nIndex= {1}", Scenario, CurrentTeamTaskIndex );
        }

        public ScenarioView Scenario { get; set; }

        public int CurrentTeamTaskIndex { get; set; }
    }
}
