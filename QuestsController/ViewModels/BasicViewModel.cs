﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace QuestsController.ViewModels
{
    public abstract class BasicViewModel<TConcreteView>
        where TConcreteView:BasicViewModel <TConcreteView>
    {
        public override bool Equals( object _other )
        {
            return Equals(_other as TConcreteView);
        }

        public bool Equals( TConcreteView _other )
        {
            if (_other == null)
                return false;

            IEnumerable < object > sequence1 = GetAttributesToIncludeInEqualityCheck();
            IEnumerable < object > sequence2 = _other.GetAttributesToIncludeInEqualityCheck();
            return sequence1.SequenceEqual(sequence2);
        }

        public static bool operator ==(BasicViewModel<TConcreteView> _left,
                                       BasicViewModel<TConcreteView> _right)
        {
            return Equals(_left, _right);
        }

        public static bool operator !=(BasicViewModel<TConcreteView> _left,
                                         BasicViewModel<TConcreteView> _right)
        {
            return !(_left == _right);
        }

        public override int GetHashCode()
        {
            return GetAttributesToIncludeInEqualityCheck().Aggregate(
                17, ( _current, _obj ) => _current * 31 + ( _obj?.GetHashCode() ?? 0 ) );
        }

        protected abstract IEnumerable<object> GetAttributesToIncludeInEqualityCheck();
    }
}
