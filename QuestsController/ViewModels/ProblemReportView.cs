﻿using System.Collections.Generic;

namespace QuestsController.ViewModels
{
	public class ProblemReportView : BasicViewModel < ProblemReportView >
	{
		public int Id { get; set; }

		public string UserName { get; set; }

		public string ProblemDescription { get; set; }


		protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
		{
			return new List < object > { Id, UserName, ProblemDescription };
		}

		public override string ToString()
		{
			return $"Id = {Id}\nUserName = {UserName}\nProblemDescription: {ProblemDescription}";
		}
	}
}