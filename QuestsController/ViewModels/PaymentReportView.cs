﻿using System.Collections.Generic;

namespace QuestsController.ViewModels
{
	public class PaymentReportView : BasicViewModel < PaymentReportView >
	{
		public int Id { get; set; }

		public string UserName { get; set; }

		public string Destination { get; set; }

		public decimal Size { get; set; }


		public override string ToString()
		{
			return $"ID = {Id}\nUser Name = {UserName}\nDestination = {Destination}\nSize = {Size}";
		}

		protected override IEnumerable < object > GetAttributesToIncludeInEqualityCheck()
		{
			return new List < object > { Id, UserName, Destination, Size };
		}
	}
}