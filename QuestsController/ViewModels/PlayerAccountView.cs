﻿
using System.Collections.Generic;


namespace QuestsController.ViewModels
{
    public class PlayerAccountView:BasicViewModel <PlayerTaskView>
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual VerifierView Verificator { get; set; }

        public virtual ICollection<MessageView> UserMessage { get; set; }

        public int PlayerRating { get; set; }

        public ICollection<PaymentReportView> PaymentHistory { get; set; }

        public ICollection<GameEventView> EventHistory { get; set; }


        protected override IEnumerable<object> GetAttributesToIncludeInEqualityCheck()
        {
            var list = new List<object> { Id, Name, PlayerRating, Verificator}; 
            list.AddRange( UserMessage );
            list.AddRange( PaymentHistory );
            list.AddRange( EventHistory );
            return list;
        }

        public override string ToString()
        {
            return string.Format( "Id= {0}\nName= {1}\nPlayer Rating= {2}\n" +
                                  "User Message= {3}\nPayment History= {4}\nEvent History= {5}\n",
                                  Id,Name,PlayerRating,string.Join( ",",UserMessage ),
                                  string.Join( ",",PaymentHistory ),string.Join( ",",EventHistory ));
        }
    }
}
