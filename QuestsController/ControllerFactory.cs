﻿using QuestsController.Implementations;
using QuestsController.Interfaces;

namespace QuestsController
{
	public static class ControllerFactory
	{
		public static IAccountManagerController MakeAccountManagerController()
		{
			return new AccountManagerController();
		}

		public static IScenariosBaseController MakeScenariosBaseController()
		{
			return new ScenariosBaseController();
		}

		public static IScheduleController MakeScheduleController()
		{
			return new ScheduleController();
		}

		public static IEventController MakeEventController()
		{
			return new EventController();
		}

		public static IGameEventController MakeGameEventController()
		{
			return new GameEventController();
		}

		public static IUserAccountController MakeUserAccountController()
		{
			return new UserAccountController();
		}

		public static IOperatorAccountController MakeOperatorAccountController()
		{
			return new OperatorAccountController();
		}

		public static IPlayerAccountController MakePlayerAccountController()
		{
			return new PlayerAccountController();
		}

        public static IScenarioController MakeScenarioController()
        {
            return new ScenarioController();
        }
	}
}