﻿using QuestsOrm.Implementations;
using QuestsOrm.Interfaces;

namespace QuestsOrm
{
	public static class RepositoryFactory
	{
		public static IUserAccountsRepository MakeUserAccountsRepository( QuestsDatabaseFacade _databaseContext )
		{
			return new UserAccountsRepository( _databaseContext );
		}

		public static IScenariosRepository MakeScenariosRepository( QuestsDatabaseFacade _databaseContext )
		{
			return new ScenariosRepository( _databaseContext );
		}

		public static IEventRepository MakeEventRepository( QuestsDatabaseFacade _databaseContext )
		{
			return new EventRepository( _databaseContext );
		}
	}
}