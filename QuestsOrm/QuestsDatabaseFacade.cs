﻿using System.Data.Entity;
using QuestsLogic.Accounts;
using QuestsLogic.Scenarios;
using QuestsLogic.Schedule.Events;
using QuestsOrm.Configurations;

namespace QuestsOrm
{
	public class QuestsDatabaseFacade : DbContext
	{
		static QuestsDatabaseFacade()
		{
			Database.SetInitializer( new DropCreateDatabaseAlways< QuestsDatabaseFacade >() );
		}


		protected override void OnModelCreating ( DbModelBuilder _modelBuilder )
		{
			_modelBuilder.Configurations.Add( new UserAccountConfiguration() );
			_modelBuilder.Configurations.Add( new OperatorAccountConfiguration() );
			_modelBuilder.Configurations.Add( new PlayerAccountConfiguration() );
			_modelBuilder.Configurations.Add( new VerifierConfiguration() );

			_modelBuilder.Configurations.Add( new BaseEventConfiguration() );
			_modelBuilder.Configurations.Add( new GameEventConfiguration() );
			_modelBuilder.Configurations.Add( new MessageConfiguration() );
			_modelBuilder.Configurations.Add( new PaymentReportConfiguration() );
			_modelBuilder.Configurations.Add( new PlayersTeamConfiguration() );
			_modelBuilder.Configurations.Add( new PlayerTaskConfiguration() );
			_modelBuilder.Configurations.Add( new ProblemReportConfiguration() );
			_modelBuilder.Configurations.Add( new ScenarioConfiguration() );
			_modelBuilder.Configurations.Add( new TeamProgressConfiguration() );
		}


		public DbSet< UserAccount > UserAccounts { get; set; }
		public DbSet< Scenario > Scenarios{ get; set; }
		public DbSet< BaseEvent > Events{ get; set; }
	}
}