﻿using QuestsOrm.Interfaces;
using QuestsLogic.Schedule.Events;

namespace QuestsOrm.Implementations
{
	public class EventRepository : BasicRepository < BaseEvent >, IEventRepository
	{
		public EventRepository( QuestsDatabaseFacade _databaseContext )
			:	base( _databaseContext, _databaseContext.Events )
		{

		}


		public void RemoveEvent( BaseEvent _event )
		{
			QuestsDatabaseFacade databaseContext = GetDatabaseContext();

			databaseContext.Events.Remove( _event );
		}
	}
}