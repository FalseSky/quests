﻿using System.Linq;

using QuestsOrm.Interfaces;
using QuestsLogic.Accounts;

namespace QuestsOrm.Implementations
{
	public class UserAccountsRepository : BasicRepository < UserAccount >, IUserAccountsRepository
	{
		public UserAccountsRepository( QuestsDatabaseFacade _databaseContext )
			:	base( _databaseContext, _databaseContext.UserAccounts )
		{

		}


		public UserAccount FindByName( string _userName )
		{
			QuestsDatabaseFacade databaseContext = GetDatabaseContext();

			return databaseContext.UserAccounts.SingleOrDefault( _userAccount => _userAccount.Name == _userName );
		}


		public void RemoveByName( string _userName )
		{
			QuestsDatabaseFacade databaseContext = GetDatabaseContext();

			UserAccount userAccount = FindByName( _userName );
			if( userAccount == null )
				return;

			databaseContext.UserAccounts.Remove( userAccount );
		}
	}
}