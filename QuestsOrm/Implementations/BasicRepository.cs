﻿using System.Data.Entity;
using System.Linq;

namespace QuestsOrm.Implementations
{
	public class BasicRepository < T > where T : class
	{
		protected BasicRepository( QuestsDatabaseFacade _databaseContext, DbSet < T > _databaseSet )
		{
			m_databaseContext = _databaseContext;

			m_databaseSet = _databaseSet;
		}
		

		public void Add( T _item )
		{
			m_databaseSet.Add( _item );
		}
		
		public T Load( int _itemId )
		{
			return m_databaseSet.Find( _itemId );
		}

		public IQueryable < T > LoadAll()
		{
			return m_databaseSet;
		}
		
		public void Commit()
		{
			m_databaseContext.ChangeTracker.DetectChanges();
			m_databaseContext.SaveChanges();
		}
		

		protected QuestsDatabaseFacade GetDatabaseContext()
		{
			return m_databaseContext;
		}
		
		protected DbSet GetDatabaseSet()
        {
            return m_databaseSet;
        }


		private readonly QuestsDatabaseFacade m_databaseContext;
		private readonly DbSet < T > m_databaseSet;
	}
}