﻿using System.Linq;
using QuestsLogic.Scenarios;
using QuestsOrm.Interfaces;

namespace QuestsOrm.Implementations
{
	public class ScenariosRepository : BasicRepository < Scenario >, IScenariosRepository
	{
		public ScenariosRepository( QuestsDatabaseFacade _databaseContext )
			:	base( _databaseContext, _databaseContext.Scenarios )
		{

		}


		public Scenario FindByName( string _scenarioName )
		{
			QuestsDatabaseFacade databaseContext = GetDatabaseContext();

			return databaseContext.Scenarios.SingleOrDefault( _scenario => _scenario.Name == _scenarioName );
		}


		public void RemoveByName( string _scenarioName )
		{
			QuestsDatabaseFacade databaseContext = GetDatabaseContext();

			Scenario scenario = FindByName( _scenarioName );
			if( scenario == null )
				return;

			databaseContext.Scenarios.Remove( scenario );
		}
	}
}