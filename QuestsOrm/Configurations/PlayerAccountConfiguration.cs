﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Accounts;

namespace QuestsOrm.Configurations
{
	public class PlayerAccountConfiguration : EntityTypeConfiguration< PlayerAccount >
	{
		public PlayerAccountConfiguration()
		{
			HasKey( _userAccount => _userAccount.Id );


			HasRequired( _userAccount => _userAccount.Verificator );

			HasOptional( _playerAccount => _playerAccount.PlayersTeam );


			HasMany( _playerAccount => _playerAccount.EventHistory );

			HasMany( _playerAccount => _playerAccount.PaymentHistory );

			HasMany( _userAccount => _userAccount.UserMessage );


			Property( _userAccount => _userAccount.Name ).IsRequired();

			Property( _userAccount => _userAccount.PlayerRating ).IsRequired();

			Property( _userAccount => _userAccount.TotalDiscount ).IsRequired();
		}
	}
}