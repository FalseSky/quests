﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Tools;

namespace QuestsOrm.Configurations
{
	public class ProblemReportConfiguration : EntityTypeConfiguration < ProblemReport >
	{
		public ProblemReportConfiguration()
		{
			HasKey( _problemReport => _problemReport.Id );


			Property( _problemReport => _problemReport.UserName ).IsRequired();
			
			Property( _problemReport => _problemReport.ProblemDescription ).IsRequired();
		}
	}
}