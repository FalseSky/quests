﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Tools;

namespace QuestsOrm.Configurations
{
	public class VerifierConfiguration : EntityTypeConfiguration < Verifier >
	{
		public VerifierConfiguration()
		{
			HasKey( _verifier => _verifier.Id );

			Property( _verifier => _verifier.KeyHash ).IsRequired();
		}
	}
}