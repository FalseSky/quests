﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Schedule.Events.GameEvent;

namespace QuestsOrm.Configurations
{
	public class GameEventConfiguration : EntityTypeConfiguration < GameEvent >
	{
		public GameEventConfiguration()
		{
			HasKey( _baseEvent => _baseEvent.Id );

			
			HasRequired( _gameEvent => _gameEvent.GameScenario );


			HasMany( _baseEvent => _baseEvent.Comments ).WithOptional();

			HasMany( _gameEvent => _gameEvent.TeamProgress );


			Property( _baseEvent => _baseEvent.Name ).IsRequired();

			Property( _baseEvent => _baseEvent.Description ).IsRequired();

			Property( _baseEvent => _baseEvent.Price ).IsRequired();

			Property( _baseEvent => _baseEvent.State ).IsRequired();

			Property( _baseEvent => _baseEvent.Time ).IsRequired();
		}
	}
}