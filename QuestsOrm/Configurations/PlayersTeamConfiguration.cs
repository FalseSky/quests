﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Schedule.Events.GameEvent;

namespace QuestsOrm.Configurations
{
	public class PlayersTeamConfiguration : EntityTypeConfiguration < PlayersTeam >
	{
		public PlayersTeamConfiguration()
		{
			HasKey( _playerTeam => _playerTeam.Id );


			HasMany( _playerTeam => _playerTeam.Players ).WithOptional();


			Property( _playerTeam => _playerTeam.Name ).IsRequired();
		}
	}
}