﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Accounts;

namespace QuestsOrm.Configurations
{
	public class UserAccountConfiguration : EntityTypeConfiguration < UserAccount >
	{
		public UserAccountConfiguration()
		{
			HasKey( _userAccount => _userAccount.Id );

			HasRequired( _userAccount => _userAccount.Verificator );

			Property( _userAccount => _userAccount.Name ).IsRequired();

			HasMany( _userAccount => _userAccount.UserMessage ).WithOptional();
		}
	}
}