﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Tools;

namespace QuestsOrm.Configurations
{
	public class MessageConfiguration : EntityTypeConfiguration< Message >
	{
		public MessageConfiguration()
		{
			HasKey( _message => _message.Id );

			Property( _message => _message.UserName ).IsRequired();
			
			Property( _message => _message.Time ).IsRequired();

			Property( _message => _message.MessageText ).IsRequired();
		}
	}
}