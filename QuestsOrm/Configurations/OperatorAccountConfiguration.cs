﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Accounts;

namespace QuestsOrm.Configurations
{
	public class OperatorAccountConfiguration : EntityTypeConfiguration < OperatorAccount >
	{
		public OperatorAccountConfiguration()
		{
			HasKey( _userAccount => _userAccount.Id );
			

			HasRequired( _userAccount => _userAccount.Verificator );


			HasMany( _userAccount => _userAccount.UserMessage ).WithRequired();

			HasMany( _operatorAccount => _operatorAccount.PaymentReports ).WithRequired();

			HasMany( _operatorAccount => _operatorAccount.ProblemReports ).WithRequired();
			

			Property( _userAccount => _userAccount.Name ).IsRequired();
		}
	}
}