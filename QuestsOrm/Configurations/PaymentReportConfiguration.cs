﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Tools;

namespace QuestsOrm.Configurations
{
	public class PaymentReportConfiguration : EntityTypeConfiguration < PaymentReport >
	{
		public PaymentReportConfiguration()
		{
			HasKey( _paymentReport => _paymentReport.Id );


			Property( _paymentReport => _paymentReport.UserName ).IsRequired();

			Property( _paymentReport => _paymentReport.Size ).IsRequired();

			Property( _paymentReport => _paymentReport.Destination ).IsRequired();
		}
	}
}