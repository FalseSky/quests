﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Schedule.Events;

namespace QuestsOrm.Configurations
{
	public class BaseEventConfiguration : EntityTypeConfiguration < BaseEvent >
	{
		public BaseEventConfiguration()
		{
			HasKey( _baseEvent => _baseEvent.Id );


			HasMany( _baseEvent => _baseEvent.Comments ).WithOptional();


			Property( _baseEvent => _baseEvent.Name ).IsRequired();

			Property( _baseEvent => _baseEvent.Description ).IsRequired();

			Property( _baseEvent => _baseEvent.Price ).IsRequired();

			Property( _baseEvent => _baseEvent.State ).IsRequired();

			Property( _baseEvent => _baseEvent.Time ).IsRequired();
		}
	}
}