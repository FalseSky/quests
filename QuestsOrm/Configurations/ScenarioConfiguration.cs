﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Scenarios;

namespace QuestsOrm.Configurations
{
	public class ScenarioConfiguration : EntityTypeConfiguration < Scenario >
	{
		public ScenarioConfiguration()
		{
			HasKey( _scenario => _scenario.Id );


			HasMany( _scenario => _scenario.PlayerTasks ).WithRequired();


			Property( _scenario => _scenario.Name ).IsRequired();

			Property( _scenario => _scenario.MaximumPlayers ).IsRequired();

			Property( _scenario => _scenario.MinimumPlayers ).IsRequired();
		}
	}
}