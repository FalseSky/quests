﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Schedule.Events.GameEvent;

namespace QuestsOrm.Configurations
{
	public class TeamProgressConfiguration : EntityTypeConfiguration < TeamProgress >
	{
		public TeamProgressConfiguration()
		{
			HasKey( _teamProgres => _teamProgres.Id );


			HasRequired( _teamProgres => _teamProgres.PlayersTeam );

			HasRequired( _teamProgres => _teamProgres.Scenario );


			Property( _teamProgres => _teamProgres.CurrentTeamTaskIndex ).IsRequired();
		}
	}
}