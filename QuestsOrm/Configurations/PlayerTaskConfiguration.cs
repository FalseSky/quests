﻿using System.Data.Entity.ModelConfiguration;
using QuestsLogic.Scenarios.Tasks;

namespace QuestsOrm.Configurations
{
	public class PlayerTaskConfiguration : EntityTypeConfiguration < PlayerTask >
	{
		public PlayerTaskConfiguration()
		{
			HasKey( _playerTask => _playerTask.Id );


			HasRequired( _playerTask => _playerTask.CompleteTaskVerificator );


			Property( _playerTask => _playerTask.Description ).IsRequired();
		}
	}
}