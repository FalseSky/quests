﻿using QuestsLogic.Schedule.Events;

namespace QuestsOrm.Interfaces
{
	public interface IEventRepository : IRepository < BaseEvent >
	{
		void RemoveEvent( BaseEvent _event );
	}
}