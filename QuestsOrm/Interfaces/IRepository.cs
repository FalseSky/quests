﻿using System.Linq;

namespace QuestsOrm.Interfaces
{
	public interface IRepository< T > where T : class
	{
		T Load( int _itemId );

		IQueryable < T > LoadAll();

		void Add( T _item );

		void Commit();
	}
}