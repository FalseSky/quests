﻿using QuestsLogic.Accounts;

namespace QuestsOrm.Interfaces
{
	public interface IUserAccountsRepository : IRepository < UserAccount >
	{
		UserAccount FindByName( string _userName );

		void RemoveByName( string _userName );
	}
}