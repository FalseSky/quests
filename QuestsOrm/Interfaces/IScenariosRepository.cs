﻿using QuestsLogic.Scenarios;

namespace QuestsOrm.Interfaces
{
	public interface IScenariosRepository : IRepository < Scenario >
	{
		Scenario FindByName( string _userName );

		void RemoveByName( string _userName );
	}
}