﻿using System;
using NUnit.Framework;
using QuestsLogic.Scenarios;
using QuestsLogic.Schedule.Events;
using QuestsLogic.Schedule.Events.EventExceptions;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsLogic.Schedule.Events.GameEvent.GameEventExceptions;

namespace QuestsTests
{
	[ TestFixture ]
	public class GameEventTests
	{
		[ Test ]
		public void StateIsNotStartedByDefault()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			Assert.IsTrue( gameEvent.State == EventState.NotStarted );
		}

		[ Test ]
		public void StateChangedIfStartEvent()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();

			Assert.IsTrue( gameEvent.State == EventState.Started );
		}

		[ Test ]
		public void StateChangedIfStartEventThanEventPaused()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.Pause();
			gameEvent.Start();

			Assert.IsTrue( gameEvent.State == EventState.Started );
		}

		[ Test ]
		public void ThrowIfStartSecondTime()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );
			gameEvent.Start();

			Assert.Throws< EventStartedException >( () => gameEvent.Start() );
		}

		[ Test ]
		public void ThrowIfStartAfterEnd()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.End();

			Assert.Throws< EventEndedException >( () => gameEvent.Start() );
		}

		[ Test ]
		public void ThrowIfPauseIfGameNotStarted()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			Assert.Throws< EventNotStartedException >( () => gameEvent.Pause() );
		}

		[ Test ]
		public void ThrowIfPauseIfGamePaused()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );
	
			gameEvent.Start();
			gameEvent.Pause();

			Assert.Throws< EventPausedException >( () => gameEvent.Pause() );
		}

		[ Test ]
		public void ThrowIfPauseIfGameEnded()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.End();

			Assert.Throws< EventEndedException >( () => gameEvent.Pause() );
		}

		[ Test ]
		public void StateChangedIfPauseEvent()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.Pause();

			Assert.IsTrue( gameEvent.State == EventState.Paused );
		}

		[ Test ]
		public void ThrowIfEndIfGameNotStated()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			Assert.Throws< EventNotStartedException >( () => gameEvent.End() );
		}

		[ Test ]
		public void ThrowIfEndIfGameEnded()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.End();

			Assert.Throws< EventEndedException >( () => gameEvent.End() );
		}

		[ Test ]
		public void StateChangedIfEndEventThanEventStarted()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.End();

			Assert.IsTrue( gameEvent.State == EventState.Ended );
		}

		[ Test ]
		public void StateChangedIfEndEventThanEventPaused()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.Pause();
			gameEvent.End();

			Assert.IsTrue( gameEvent.State == EventState.Ended );
		}

		[ Test ]
		public void ThrowIfAddTeamIfGameStarted()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();

			Assert.Throws < EventStartedException >( () => gameEvent.AddTeam( new PlayersTeam( "Team" ) ) );
		}
		
		[ Test ]
		public void ThrowIfAddTeamIfGamePaused()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.Pause();

			Assert.Throws < EventStartedException >( () => gameEvent.AddTeam( new PlayersTeam( "Team" ) ) );
		}
		
		[ Test ]
		public void ThrowIfAddTeamIfGameEnded()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			gameEvent.Start();
			gameEvent.End();

			Assert.Throws < EventStartedException >( () => gameEvent.AddTeam( new PlayersTeam( "Team" ) ) );
		}
		
		[ Test ]
		public void ThrowIfRemoveTeamIfGameStarted()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			PlayersTeam playersTeam = new PlayersTeam( "Team" );
			gameEvent.AddTeam( playersTeam );

			gameEvent.Start();

			Assert.Throws < EventStartedException >( () => gameEvent.RemoveTeam( playersTeam ) );
		}
		
		[ Test ]
		public void ThrowIfRemoveTeamIfGamePaused()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );
			
			PlayersTeam playersTeam = new PlayersTeam( "Team" );
			gameEvent.AddTeam( playersTeam );

			gameEvent.Start();
			gameEvent.Pause();

			Assert.Throws < EventStartedException >( () => gameEvent.RemoveTeam( playersTeam ) );
		}
		
		[ Test ]
		public void ThrowIfRemoveTeamIfGameEnded()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );
			
			PlayersTeam playersTeam = new PlayersTeam( "Team" );
			gameEvent.AddTeam( playersTeam );

			gameEvent.Start();
			gameEvent.End();

			Assert.Throws < EventStartedException >( () => gameEvent.RemoveTeam( playersTeam ) );
		}
		
		[ Test ]
		public void ThrowIfRemoveTeamWhatNotExist()
		{
			GameEvent gameEvent = new GameEvent( DateTime.Now, "Game", "Game", 0, new Scenario( "Scenario" ) );

			Assert.Throws < InvalidOperationException >( () => gameEvent.RemoveTeam( new PlayersTeam( "Team" ) ) );
		}
	}
}