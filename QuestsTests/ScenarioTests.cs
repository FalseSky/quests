﻿using System;
using NUnit.Framework;
using QuestsLogic.Scenarios;
using QuestsLogic.Scenarios.ScenarioExceptions;
using QuestsLogic.Scenarios.ScenariosExceptions;
using QuestsLogic.Scenarios.Tasks;

namespace QuestsTests
{
	[ TestFixture ]
	public class ScenarioTests
	{
		[ Test ]
		public void ThrowIfMinimumPlayerIsLessThanOne()
		{
			Scenario scenario = new Scenario( "Scenario" );
			Assert.Throws < MinimumNumberLessThanOneException >( () => scenario.MinimumPlayers = 0 );
		}

		[ Test ]
		public void ThrowIfMaximumPlayerIsLessThanMinimumPlayer()
		{
			Scenario scenario = new Scenario( "Scenario" );
			scenario.MinimumPlayers = 10;

			Assert.Throws < MaximumNumberLessThanMinimumException >( () => scenario.MaximumPlayers = 5 );
		}
	}
}