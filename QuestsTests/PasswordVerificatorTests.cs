﻿using NUnit.Framework;
using QuestsLogic.Tools;
using QuestsLogic.Tools.VerifierExceptions;

namespace QuestsTests
{
	[ TestFixture ]
	public class PasswordVerificatorTests
	{
		[ Test ]
        public void ThrowIfKeyIsInvalid()
		{
			Verifier passwordVerificator = new Verifier( "ValidPassword" );
			Assert.Throws< InvalidKeyException >( () => passwordVerificator.ThrowIfKeyInvalid( "WrongPassword" ) );
		}
		
		[ Test ]
        public void NoThrowIfKeyIsValid()
		{
			Verifier passwordVerificator = new Verifier( "ValidPassword" );
			Assert.DoesNotThrow( () => passwordVerificator.ThrowIfKeyInvalid( "ValidPassword" ) );
		}

		[ Test ]
        public void IsValidReturnTrueIfPasswordIsValid()
		{
			Verifier passwordVerificator = new Verifier( "ValidPassword" );
			Assert.IsTrue( passwordVerificator.ValidKey( "ValidPassword" ) );
        }

		[ Test ]
        public void IsValidReturnFalseIfPasswordIsInvalid()
		{
			Verifier passwordVerificator = new Verifier( "ValidPassword" );
			Assert.IsFalse( passwordVerificator.ValidKey( "WrongPassword" ) );
        }
	}
}