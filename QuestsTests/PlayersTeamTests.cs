﻿using System.Collections.Generic;
using NUnit.Framework;
using QuestsLogic.Accounts;
using QuestsLogic.Accounts.PlayerAccountExceptions;
using QuestsLogic.Schedule.Events.GameEvent;
using QuestsLogic.Schedule.Events.GameEvent.PlayerTeamExceptions;
using QuestsLogic.Tools;

namespace QuestsTests
{
	[ TestFixture ]
	public class PlayersTeamTests
	{
		[ Test ]
		public void ThrowIfAddNullPlayer()
		{
			PlayersTeam playersTeam = new PlayersTeam( "TeamName" );
			Assert.Throws < NullPlayerException >( () => playersTeam.AddPlayer( null ) );
		}
	}
}